﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using EmbeddedWebserver.Core.Configuration;
using EmbeddedWebserver.Core;
using EmbeddedWebserver.Core.Helpers;
using MicroLibrary.Time;

namespace SensorServer
{
    public class Program
    {
        public static Sensors Sensor;
        static OutputPort led = new OutputPort(Pins.ONBOARD_LED, false);
        public static void Main()
        {
            Ntp.UpdateTimeFromNtpServer();
            Sensor = new Sensors();

            // write your code here
            EmbeddedWebapplicationConfiguration config = new EmbeddedWebapplicationConfiguration();
            config.MaxWorkerThreadCount = 10;
            config.FileSystemHandlerEnableDirectoryBrowsing = true;
            config.Port = 80;

            Microsoft.SPOT.Net.NetworkInformation.NetworkInterface[] networkInterfaces = Microsoft.SPOT.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
            foreach (var networkInterface in networkInterfaces)
            {
                DebugHelper.Print("Ip adress: " + networkInterface.IPAddress);
            }


            using (Webserver server = new Webserver(config))
            {
                //MicroLibrary.Pulsate.Pulse(SecretLabs.NETMF.Hardware.NetduinoPlus.Pins.ONBOARD_LED, 1000);

                server.RegisterHandler("sensor", typeof(SensorHandler));
                server.OnRequestProcessed += new Webserver.RequestProcessed(server_OnRequestProcessed);
                
                server.StartListening();
                while (true)
                {
                    
                    Thread.Sleep(500);
                }
                //Thread.Sleep(System.Threading.Timeout.Infinite);
            }

        }

        static void server_OnRequestProcessed(HttpContext Context)
        {
            lock (led)
            {
                string msg = "Processed:";
                if (Context != null) msg += " " + Context.Request.Url;
                Debug.Print(msg);
                led.Write(!led.Read());
            }
        }

    }
}