using System;
using Microsoft.SPOT;
using MicroLibrary.Sensors.Motion;
using MicroLibrary.Interfaces;

namespace SensorServer
{
    public class Sensors : IThingSpeak
    {
        public MotionSensorData LastEntry { get; set; }
        public Sensors()
        {
            this.ThingWriteAPIKey = "NP2EJJG9RAYL1YEO";
            LastEntry = new MotionSensorData();
            LastEntry.Date = DateTime.MinValue;
            LastEntry.Sensor = null;
            LastEntry.Value = 0;
            IMotionSensor sensor = new Panasonic1111(SecretLabs.NETMF.Hardware.NetduinoPlus.Pins.GPIO_PIN_A1);
            LastEntry.Threshold = sensor.Threshold;
            //sensor.Threshold = 500;
            sensor.OnMotionDetected += new MotionDetected(sensor_OnMotionDetected);
        }

        void sensor_OnMotionDetected(MotionSensorData Data)
        {
            LastEntry = Data;
            MicroLibrary.ThingSpeak.Controller.Write(this);
        }
        public DateTime ThingDate { get; set; }
        public string ThingWriteAPIKey
        {
            get;
            set;
        }

        public bool ThingWrite
        {
            get
            {
                return LastEntry != null && LastEntry.Date > DateTime.MinValue && LastEntry.Value > 0;
            }
        }

        public double ThingValue
        {
            get { return LastEntry.Value; }
        }
    }
}