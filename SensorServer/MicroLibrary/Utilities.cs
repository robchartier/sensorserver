using System;
using Microsoft.SPOT;
using System.Threading;

namespace MicroLibrary
{
    public class Utilities
    {
        /// <summary>
        /// Block the thread forever
        /// </summary>
        /// <returns></returns>
        public static Thread Wait()
        {
            while (true)
            {
                Thread.Sleep(10);
            }
        }
        public delegate void WaitDelegate();

        public static Thread WaitAndExecute(WaitDelegate ExecuteMe, int Sleep)
        {
            while (true)
            {
                Thread.Sleep(Sleep);
                ExecuteMe();
            }
        }
    }
}
