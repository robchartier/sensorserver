using System;
using Microsoft.SPOT;
using System.Net;
using System.IO;
using System.Collections;

namespace MicroLibrary.Net
{
    public class Http
    {
        public static void Post(string Url, byte[] Data, Hashtable Headers = null)
        {
            WebRequest request = HttpWebRequest.Create(Url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = Data.Length;
            if (Headers != null && Headers.Count > 0)
            {
                foreach (string key in Headers.Keys)
                {
                    request.Headers.Add(key, Headers[key].ToString());
                }
            }
            using (Stream postDataStream = request.GetRequestStream())
            {

            }
        }
    }
}
