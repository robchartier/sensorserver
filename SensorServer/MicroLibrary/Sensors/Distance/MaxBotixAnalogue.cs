//           Ascended International
//
//             Copyright (c) 2010
//
//            All Rights Reserved
// ------------------------------------------------------------------------------
//    * DO NOT REMOVE THIS HEADER. DO NOT MODIFY THIS HEADER *
/**********************************************************************************
 * You may use this class for non-commerical purposes. If you wish to use this    *
 * software for a commercial pupose please contact Ascended International at:     *
 * mark@MicroLibrary.com.au                                                           *
 *                                                                                *
 * When using this free class, no warranty express or implied is offered.         *
 * In no event shall Ascended International, any of it's employees or shareholds  *
 * be held liable for any direct, indirect, special, exemplary, incidental or     *
 * consequential damages however caused.                                          *
 *                                                                                *
 * If you modify this class, please add your name and the date below as a         *
 * contributor, without removing this notice or modifying it in any way, shape    *
 * or form.                                                                       *
 **********************************************************************************/
/*
 Contributors:
 * 16 Sep 2010, Mark Harris - Initial code, maths and development [ markh@rris.com.au ]
 * 
 */

using System;
using Microsoft.SPOT;
//using GHIElectronics.NETMF.Hardware;
//using GHIElectronics.NETMF.System;
using MicroLibrary.Sensors.Interfaces;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;

namespace MicroLibrary.Sensors.Distance
{
    public class MaxBotixAnalogue : DistanceSensorBase
    {
        public enum SupplyVoltage
        {
            ThreePointThreeVolt,
            FiveVolt
        }

        SupplyVoltage voltage;

        public MaxBotixAnalogue(Cpu.Pin pin, SupplyVoltage volt)
        {
            voltage = volt;

            sensor = new AnalogInput(pin);
            sensor.SetRange(0, 3300);

            UseMovingAverage = false;
            MovingAverageSize = 5;

            UseAveraging = true;
            AverageSampleCount = 10;

            movingValues = new double[MovingAverageSize];
            FillMovingAverageValues();
        }

        protected override double ReadSensorValue()
        {
            int x = sensor.Read();

            int supply = 3300;
            if (voltage == SupplyVoltage.FiveVolt)
                supply = 5000;

            double mvPerIn = supply / 512d;

            return (x / mvPerIn) * 2.54;
        }
    }
}
