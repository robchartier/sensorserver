using System;
using Microsoft.SPOT;
using MicroLibrary.Sensors.Interfaces;
using System.Threading;

namespace MicroLibrary.Sensors
{
    public class DistanceSensorMonitor
    {
        public delegate void Change(IDistanceSensor Sensor, double Threshold, double LastValue, double NewValue);
        public event Change OnChange;

        public IDistanceSensor Sensor { get; set; }

        /// <summary>
        /// The maximum value to trigger off of. 
        /// This should prevent triggers when the sensed object is removed quickly
        /// </summary>
        public double MaxValue { get; set; }

        /// <summary>
        /// How large of a difference can the sensor read before we notify
        /// </summary>
        public double Threshold { get; set; }
        
        /// <summary>
        /// The value from the sensor the last time the value was triggered.
        /// </summary>
        public double LastValue { get; set; }

        private System.Threading.Thread watchThread;
        public void MonitorSensor(IDistanceSensor Sensor, double Threshold)
        {
            MaxValue = 1000;
            this.Sensor = Sensor;
            this.Threshold = Threshold;
            watchThread = new Thread(new System.Threading.ThreadStart(threadMonitor));
            watchThread.Start();
        }
        private void threadMonitor()
        {
            while (true)
            {
                double currentValue = Sensor.GetDistanceInCM();
                if (currentValue <= MaxValue)
                {
                    double diff = Math.Abs(currentValue - LastValue);
                    if (diff > Threshold && currentValue > 0)
                    {
                        if (OnChange != null) OnChange(Sensor, Threshold, LastValue, currentValue);
                        LastValue = currentValue;
                    }
                }
                Thread.Sleep(100);   
            }
        }
    }
}
