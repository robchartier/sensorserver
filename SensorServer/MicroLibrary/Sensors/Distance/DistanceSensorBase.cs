//           Ascended International
//
//             Copyright (c) 2010
//
//            All Rights Reserved
// ------------------------------------------------------------------------------
//    * DO NOT REMOVE THIS HEADER. DO NOT MODIFY THIS HEADER *
/**********************************************************************************
 * You may use this class for non-commerical purposes. If you wish to use this    *
 * software for a commercial pupose please contact Ascended International at:     *
 * mark@MicroLibrary.com.au                                                           *
 *                                                                                *
 * When using this free class, no warranty express or implied is offered.         *
 * In no event shall Ascended International, any of it's employees or shareholds  *
 * be held liable for any direct, indirect, special, exemplary, incidental or     *
 * consequential damages however caused.                                          *
 *                                                                                *
 * If you modify this class, please add your name and the date below as a         *
 * contributor, without removing this notice or modifying it in any way, shape    *
 * or form.                                                                       *
 **********************************************************************************/
/*
 Contributors:
 * 16 Sep 2010, Mark Harris - Initial code, maths and development [ markh@rris.com.au ]
 * 
 */

using System;
using Microsoft.SPOT;
//using GHIElectronics.NETMF.Hardware;
//using GHIElectronics.NETMF.System;
using MicroLibrary.Sensors.Interfaces;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
namespace MicroLibrary.Sensors.Distance
{
    public abstract class DistanceSensorBase : IDistanceSensor
    {


        protected AnalogInput sensor;

        protected double[] movingValues;
        int movingPos;

        double[] data;
        double[] values;

        /// <summary>
        /// This should not be used as there is no way to set the pin your sensor is on. This is here for inheritance.
        /// </summary>
        public DistanceSensorBase()
        {

        }

        /// <summary>
        /// Create a new instance of Sharp2D120X. You should sleep for *at least* 50ms after instanciating this class.
        /// The sensor does not produce accurate results for at least this time.
        /// </summary>
        /// <param name="pin">Pin the sensor's vout is connected to.</param>
        public DistanceSensorBase(Cpu.Pin pin)
        {
            sensor = new AnalogInput(pin);
            sensor.SetRange(0, 3300);

            UseMovingAverage = false;
            MovingAverageSize = 5;

            UseAveraging = true;
            AverageSampleCount = 10;

            movingValues = new double[MovingAverageSize];
            FillMovingAverageValues();
        }

        /// <summary>
        /// Returns the distance in centimetres and will apply any filtering or averaging before returning the value.
        /// </summary>
        /// <returns>The distance the sensor is reading in CM.</returns>
        public double GetDistanceInCM()
        {
            double distance = 0;

            if (UseAveraging)
            {
                data = new double[AverageSampleCount];

                double min = Double.MaxValue;
                double max = Double.MinValue;

                for (int i = 0; i < AverageSampleCount; i++)
                {
                    data[i] = ReadSensorValue();
                    distance += data[i];

                    if (UseFiltering)
                    {
                        if (min > data[i])
                            min = data[i];

                        if (max < data[i])
                            max = data[i];
                    }
                }

                distance /= AverageSampleCount;

                if (UseFiltering)
                {
                    // do we need to do cleanup through deviation?
                    if (!(max == min || max - min < 1))
                    {
                        // trim down the data
                        double stdDev = StandardDeviation(data, distance);

                        // Standard Dev is too high for our liking!
                        if (stdDev > 0.5)
                        {
                            int candidates = 0;

                            values = new double[AverageSampleCount];

                            for (int i = 0; i < AverageSampleCount; i++)
                            {
                                if (data[i] < distance + stdDev && data[i] > distance - stdDev)
                                    values[candidates++] = data[i];
                            }

                            distance = Average(values, candidates - 1);
                        }
                    }
                }
            }
            else
            {
                distance = ReadSensorValue();
            }

            if (UseMovingAverage)
            {
                AddMovingAverage(distance);
                return Average(movingValues);
            }

            return distance;
        }

        protected abstract double ReadSensorValue();

        double StandardDeviation(double[] data, double avg)
        {
            //double avg = 0;
            double totalVariance = 0;
            int max = data.Length;

            if (max == 0)
                return 0;

            // get variance
            for (int i = 0; i < max; i++)
                totalVariance += System.Math.Pow(data[i] - avg, 2);

            return MicroLibrary.Math.Sqrt(totalVariance / max);
        }

        double Average(double[] data)
        {
            return Average(data, data.Length);
        }

        double Average(double[] data, int count)
        {
            double avg = 0;

            for (int i = 0; i < data.Length; i++)
                avg += data[i];

            if (avg == 0 || data.Length == 0)
                return 0;

            return avg / data.Length;
        }

        void AddMovingAverage(double nextValue)
        {
            movingValues[movingPos++] = nextValue;

            if (movingPos >= movingValues.Length)
                movingPos = 0;
        }

        protected void FillMovingAverageValues()
        {
            for (int i = 0; i < movingAverageSize; i++)
            {
                AddMovingAverage(ReadSensorValue());
            }
        }

        bool useMovingAverage;
        /// <summary>
        /// Controls whether the system uses moving averages to smooth out values.
        /// This should be True if you have a high speed update, or are movement is slow.
        /// This should be False if you are moving fast or have a slow update rate.
        /// Moving averages introduce some lag into the system, therefore it's only useful with high update speeds or slow movements.
        /// The moving average can take out spikes and other crazy phenomenom efficiently.
        /// </summary>
        public bool UseMovingAverage
        {
            get { return useMovingAverage; }
            set 
            {
                if (useMovingAverage == value)
                    return;

                useMovingAverage = value;

                if (useMovingAverage == true)
                {
                    movingValues = new double[movingAverageSize];
                    movingPos = 0;

                    FillMovingAverageValues();
                }
            }
        }
        
        int movingAverageSize;
        /// <summary>
        /// Gets/Sets the size of the moving average set. If the set is too large, there will be considerable lag. If the set is too small, it will be inefficient.
        /// </summary>
        public int MovingAverageSize
        {
            get { return movingAverageSize; }
            set 
            {
                if (movingAverageSize == value)
                    return;

                movingAverageSize = value;

                movingValues = new double[movingAverageSize];
                movingPos = 0;

                FillMovingAverageValues();
            }
        }

        /// <summary>
        /// Gets/Sets whether standard deviation filtering should be used. This only works with UseAveraging turned on. 
        /// If values read have too wide of a range, anything beyond one standard deviation will be culled and ignored.
        /// If you have high speed movement, this will get a good workout and may not be of any benefit.
        /// If you have slow movement with a lot of 'noise' this will clean it up considerably.
        /// </summary>
        public bool UseFiltering { get; set; }

        /// <summary>
        /// When using averaging, the sensor will take <c>AverageSampleCount</c> samples and then average them to remove some fluctuations.
        /// This works very well at all times and should be left on, vary the sample count based on the speed of code you require, and the speed you move at.
        /// </summary>
        public bool UseAveraging { get; set; }
        /// <summary>
        /// Gets/sets the number of samples to take for averaging.
        /// </summary>
        public int AverageSampleCount { get; set; }

        #region IDisposable Members

        public void Dispose()
        {
            sensor.Dispose();
        }

        #endregion
    }
}
