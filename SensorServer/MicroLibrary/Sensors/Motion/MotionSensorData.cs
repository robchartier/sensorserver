using System;
using Microsoft.SPOT;
using MicroLibrary.Interfaces;

namespace MicroLibrary.Sensors.Motion
{
    public class MotionSensorData
    {
        public IMotionSensor Sensor { get; set; }
        public int Value { get; set; }
        public int Threshold { get; set; }
        public DateTime Date { get; set; }
    }
}