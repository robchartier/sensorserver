using System;
using Microsoft.SPOT;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using Microsoft.SPOT.Hardware;
using System.Threading;

using MicroLibrary.Interfaces;

namespace MicroLibrary.Sensors.Motion
{

    public class Panasonic1111 : IMotionSensor
    {
        
        public event MicroLibrary.Sensors.Motion.MotionDetected OnMotionDetected;
        public Cpu.Pin Pin { get; set; }
        public int Threshold { get; set; }
        Timer t;
        AnalogInput sensor;
        public Panasonic1111(Cpu.Pin Pin)
        {
            Threshold = 1000;
            this.Pin = Pin;
            sensor = new AnalogInput(Pin);
            t = new Timer(new TimerCallback(update), null, 100, 50);
        }
        private void update(object state) {
            int val = sensor.Read();
            if (val > Threshold && OnMotionDetected != null)
            {
                OnMotionDetected(
                new MicroLibrary.Sensors.Motion.MotionSensorData()
                {
                    Sensor = this,
                    Value = val,
                    Threshold = Threshold,
                    Date = DateTime.Now
                });
            }
        }

    }
}
