using System;
using Microsoft.SPOT;

namespace MicroLibrary.Sensors.Motion
{
    public delegate void MotionDetected(MotionSensorData Data);
}
