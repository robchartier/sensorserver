using System;
using Microsoft.SPOT;

namespace MicroLibrary.Interfaces
{
    public interface IThingSpeak
    {
        string ThingWriteAPIKey { get; set; }
        bool ThingWrite { get; }
        double ThingValue { get; }
        DateTime ThingDate { get; }
    }
}
