﻿using System;
using Microsoft.SPOT;

namespace MicroLibrary.Sensors.Interfaces
{
    public interface IDistanceSensor : IDisposable
    {
        double GetDistanceInCM();

        bool UseMovingAverage { get; set; }
        int MovingAverageSize { get; set; }

        bool UseAveraging { get; set; }
        int AverageSampleCount { get; set; }

        bool UseFiltering { get; set; }
    }
}
