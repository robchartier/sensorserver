using System;
using Microsoft.SPOT;
using MicroLibrary.Sensors.Motion;
using Microsoft.SPOT.Hardware;

namespace MicroLibrary.Interfaces
{

    public interface IMotionSensor
    {
        event MotionDetected OnMotionDetected;
        Cpu.Pin Pin { get; set; }
        int Threshold { get; set; }
    }
}
