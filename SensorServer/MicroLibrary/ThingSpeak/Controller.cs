using System;
using Microsoft.SPOT;
using MicroLibrary.Interfaces;
using MicroLibrary.Net;
using System.Text;

namespace MicroLibrary.ThingSpeak
{
    public class Controller
    {
        public static void Write(IThingSpeak Sensor) {
            if (Sensor!=null)
            {
                if (Sensor.ThingWrite && Sensor.ThingWriteAPIKey != null && Sensor.ThingWriteAPIKey.Trim() != "")
                {
                    System.Collections.Hashtable headers = new System.Collections.Hashtable();
                    headers.Add("X-THINGSPEAKAPIKEY", Sensor.ThingWriteAPIKey);
                    string data = "key=" + Sensor.ThingWriteAPIKey + "&field1=" + Sensor.ThingValue;
                    Http.Post("http://api.thingspeak.com/update", Encoding.UTF8.GetBytes(data), headers);
                }
            }
        }
    }
}
