using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;

namespace MicroLibrary.Switchs
{
    public class MagneticSwitch : PushButton
    {
        public MagneticSwitch(Cpu.Pin pin)
            : base(pin, Port.InterruptMode.InterruptEdgeBoth, null, Port.ResistorMode.Disabled, false)
        {
            base.OnButtonStateChange += new ButtonStateChange(MagneticSwitch_OnButtonStateChange);

            t = new System.Threading.Timer(new System.Threading.TimerCallback(update), null, sleepPeriod*2, sleepPeriod);
        }
        int sleepPeriod = 50;
        System.Threading.Timer t;
        private  void update(object state) {
            TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - this.LastChange.Ticks);
            if (ts.Milliseconds > sleepPeriod && LastValue != true)
            {
                LastValue = true;
                if (OnMagneticSwitchStateChange != null) OnMagneticSwitchStateChange(this, true, DateTime.Now);
            }

        }
        DateTime lastChange = DateTime.Now;
        public DateTime LastChange { get { return lastChange; } }
        
        private bool LastValue = true;
        void MagneticSwitch_OnButtonStateChange(PushButton Sender, uint port, uint state, DateTime time)
        {
            lastChange = DateTime.Now;
            if (LastValue != false)
            {
                LastValue = false;
                if (OnMagneticSwitchStateChange != null) OnMagneticSwitchStateChange((Sender as MagneticSwitch), false, time);
            }
        }

        public delegate void MagneticSwitchStateChange(MagneticSwitch Sender, bool Connected, DateTime time);
        public event MagneticSwitchStateChange OnMagneticSwitchStateChange;

    }
}
