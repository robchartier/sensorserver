using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using System.Threading;

namespace MicroLibrary
{
    public class Pulsate
    {
        public delegate void Pulsed(Pulsate Sender);
        public event Pulsed OnPulsed;
        
        public static Pulsate Pulse(Cpu.Pin Pin, int SleepDuration)
        {
            Pulsate p = new Pulsate();
            p.Pin = Pin;
            p.SleepDuration = SleepDuration;
            p.Thread = new Thread(new ThreadStart(p._running));
            return p;
        }

        public int SleepDuration { get; set; }
        public Cpu.Pin Pin { get; set; }
        public bool Running { get; set; }
        public Thread Thread { get; set; }

        public void Stop()
        {
            if (Running)
            {
                Running = false;
                try
                {
                    this.Thread.Abort();
                }
                catch (Exception)
                {                    
                }
            }
        }
        public void Start()
        {
            if (!Running)
            {
                Running = true;
                this.Thread.Start();
            }
        }
        private void _running()
        {
            Running = true;
            PWM red = new PWM(Pin);
            while (Running)
            {
                uint x = 0;
                while (x <= 100)
                {
                    red.SetDutyCycle(x);
                    Thread.Sleep(SleepDuration);
                    x++;
                }
                x = 100;
                while (x >= 0)
                {
                    red.SetDutyCycle(x);
                    Thread.Sleep(SleepDuration);
                    x--;
                    if (x > 100) break;
                }
                if (OnPulsed != null) OnPulsed(this);
            }
        }
    }
}