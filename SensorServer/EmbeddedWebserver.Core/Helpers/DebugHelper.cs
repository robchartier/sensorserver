﻿namespace EmbeddedWebserver.Core.Helpers
{
    public static class DebugHelper
    {
        public static void Print(string pMessage)
        {
#if MF_FRAMEWORK_VERSION_V4_1
            Microsoft.SPOT.Debug.Print(pMessage);
#else
            System.Diagnostics.Debug.WriteLine(pMessage);
#endif
        }
    }
}
