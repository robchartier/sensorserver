using System;
using Microsoft.SPOT;
using EmbeddedWebserver.Core.Handlers.Abstract;
using EmbeddedWebserver.Core.Helpers;
using System.Collections;
using EmbeddedWebserver.Core;

namespace SensorServer
{
    public class SensorHandler : HandlerBase
    {
        #region Non-public members


        private static void _serializeToRow(StringBuilder pResponseBuilder, string pKey, string pValue)
        {
            pResponseBuilder.Append("<tr><td>");
            pResponseBuilder.Append(pKey);
            pResponseBuilder.Append(":</td><td>");
            pResponseBuilder.Append(pValue);
            pResponseBuilder.Append("</td></tr>");
        }

        private static void _serializeToTable(StringBuilder pResponseBuilder, StringDictionary pSourceData, string pUrlPrefix)
        {
            if (pSourceData != null && pSourceData.Count > 0)
            {
                foreach (string key in pSourceData.Keys)
                {
                    _serializeToRow(pResponseBuilder, pUrlPrefix.IsNullOrEmpty() ? key : "<a href=\"" + pUrlPrefix + key + "\">" + key + "</a>", pSourceData[key]);
                }
            }
        }

        private static void _serializeToTable(StringBuilder pResponseBuilder, StringDictionary pSourceData)
        {
            _serializeToTable(pResponseBuilder, pSourceData, null);
        }

        private static void _serializeToTable(StringBuilder pResponseBuilder, IEnumerable pSourceData)
        {
            if (pSourceData != null)
            {
                int index = 1;
                foreach (var item in pSourceData)
                {
                    _serializeToRow(pResponseBuilder, "#" + index.ToString(), (string)item);
                    index++;
                }
            }
        }

        protected override void ProcessRequestWorker(HttpContext pContext)
        {

            StringBuilder responseBuilder = new StringBuilder(HandlerBase.HtmlDoctype, 1700);
            responseBuilder.Append("<html><head><title>Current Sensor Data</title></head><body><h1>Current Sensor Data</h1><table><tr><td colspan=\"2\"><b>Motion Data</b></td></tr>");
            var last = Program.Sensor.LastEntry;
            if (last != null)
            {
                _serializeToRow(responseBuilder, "Date:", last.Date.ToString());
                _serializeToRow(responseBuilder, "Value:", last.Value.ToString());
                _serializeToRow(responseBuilder, "Threshold:", last.Threshold.ToString());
            }
            else
            {
                _serializeToRow(responseBuilder, "No Data Available", "");
            }
            responseBuilder.Append("</table></body></html>");
            pContext.Response.ResponseBody = responseBuilder.ToString();

        }

        #endregion

        #region Constructors

        public SensorHandler()
            : base(HttpMethods.GET)
        {
        }

        #endregion
    }
}